import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class BingTestWithFirefox {

    @Test
    public void testFirstBingResult(){

        System.setProperty("webdriver.gecko.driver", "E:\\Tools\\geckodriver.exe");

        WebDriver webDriver = new FirefoxDriver();

        webDriver.get("https://www.bing.com/");
        WebElement inputSearch = webDriver.findElement(By.id("sb_form_q"));
        Assert.assertTrue("Search is not displayed.", inputSearch.isDisplayed());


        WebElement tbx_search = webDriver.findElement(By.id("sb_form_q"));
        tbx_search.sendKeys("Telerik Academy Alpha");
        webDriver.findElement(By.id("search_icon")).click();


        String ActualTitle = webDriver.findElement(By.className("b_title")).getText();
        String ExpectedTitle = "IT Career Start in 6 Months - Telerik Academy Alpha";
        Assert.assertEquals(ExpectedTitle, ActualTitle);

        webDriver.quit();
    }
}
