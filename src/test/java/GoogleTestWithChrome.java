import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class GoogleTestWithChrome {
    @Test
    public void testChromeSearch(){

        System.setProperty("webdriver.chrome.driver", "E:\\Tools\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();

        webDriver.get("https://www.google.com/");




        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        webDriver.findElement(By.id("L2AGLb")).click();


        WebElement inputSearch = webDriver.findElement(By.tagName("INPUT"));
        Assert.assertTrue("Search is not displayed.", inputSearch.isDisplayed());

        inputSearch.sendKeys("Telerik Academy Alpha",Keys.ENTER);

        String ActualTitle = webDriver.findElement(By.xpath("//h3[@class='LC20lb MBeuO DKV0Md']")).getText();
        String ExpectedTitle = "IT Career Start in 6 Months - Telerik Academy Alpha";
        Assert.assertEquals(ExpectedTitle, ActualTitle);


        webDriver.quit();
    }
}
